import { createToast } from "../toasts.js";
import server from "../mockServer/server.js";


class FormResponse{
    constructor(formResponseWrapper,formId){
        this.formId = formId;
        this.formResponseWrapper = formResponseWrapper;
        this.getResponses().then(res=>{ 
            if(res.status!==200){
                return createToast({
                    heading: "Error: Cannot get responses",
                    message:res.message
                })
            }
            console.log(res.data);
            this.formData = res.data.formData;
            this.responses = res.data.responses;
            this.populateResponses();
        }).catch(err=>{
            return createToast({
                heading: "Error: Cannot get responses",
                message:err.message
            })
        })
    }
    async getResponses(){
        return await server.get({
            route:"/api/get-responses",
            params : {
                formId:this.formId
            }
        });
    }

    populateResponses(){
        let responses = this.responses;
        let mcqResponse = {};

        this.formResponseWrapper.insertAdjacentHTML('beforeend',`
            <div class="response-info">
                <div class="response-info__response-cnt">Total Responses : (${responses.length})</div>
                <button class="response-info__download-btn">Download Responses</button>
            </div>
        `);

        document.getElementsByClassName("response-info__download-btn")[0].onclick = () =>{
            this.downloadResponses();
        }

        for(let [idx,field] of this.formData.formFields.entries()){
            switch(field.fieldType){
                case "input":
                    this.insertInputResponseViewer(field,idx);
                    break;
                case "mcq":
                    this.insertMCQResponseViewer(field,idx);
                    let mcqResponseData = {
                        labels : [],
                        count : [] 
                    }
                    for(let [idx,option] of field.fieldContent.options.entries()){
                        mcqResponseData.labels[idx] = option.value;
                        mcqResponseData.count[idx] = 0; 
                    }
                    mcqResponse[field.fieldId] = mcqResponseData;
                    break;
            }
        }
        for(let response of responses){
            for (let field of response.responseData.fields){
                if(field.fieldType==="input"){
                    document.getElementById(`response-viewer-${field.fieldId}`)
                    .getElementsByClassName('input-response-holder')[0]
                    .insertAdjacentHTML('beforeend',`
                        <div class="input-response">${field.fieldValue}</div>
                    `);
                } else if(field.fieldType==="mcq"){
                    mcqResponse[field.fieldId].count[field.fieldValue]+=1;
                }
            }
        }
        import("https://cdn.jsdelivr.net/npm/chart.js").then(_=>{
            for(let [idx,field] of this.formData.formFields.entries()){
                if(field.fieldType!=="mcq") continue;
                let mcqViewer = document.getElementById("canvas-"+field.fieldId);
                console.log(mcqViewer)
                new Chart(mcqViewer, {
                    type: 'bar',
                    data: {
                      labels: mcqResponse[field.fieldId].labels,
                      datasets: [{
                        label: '# of Votes',
                        data: mcqResponse[field.fieldId].count,
                        borderWidth: 1
                      }],
                    },
                    options: {
                      scales: {
                        y: {
                          beginAtZero: true
                        }
                      },
                      layout: {
                            padding: 20
                    }
                    }
                  });
            }
        })

    }

    insertInputResponseViewer(field,idx){
        this.formResponseWrapper.insertAdjacentHTML('beforeend',`
            <div id="response-viewer-${field.fieldId}" class="input-response-viewer">
            <div class="input-question">Q${idx+1}. ${field.fieldQuestion}</div>
            <div class="input-response-holder">
            </div>
        `)
    }

    insertMCQResponseViewer(field,idx){
        this.formResponseWrapper.insertAdjacentHTML('beforeend',`
            <div id="response-viewer-${field.fieldId}" class="mcq-response-viewer">
                <div class="input-question">Q${idx+1}. ${field.fieldQuestion}</div>
                <canvas id="canvas-${field.fieldId}" class="mcq-response-canvas"/>
            </div>
        `)
    }

    downloadResponses(){
        let dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(this.responses));

        let dlAnchorElem = document.createElement('a');
        dlAnchorElem.setAttribute("href",dataStr);
        dlAnchorElem.setAttribute("download", "responses_"+this.formId+".json");
        dlAnchorElem.click();
    }


}

export default FormResponse;