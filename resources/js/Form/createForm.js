import server from "../mockServer/server.js"
import { createToast } from "../toasts.js";
class Form{

    constructor(formWrapper){
        this.formWrapper = formWrapper;
        this.isFieldCreatorOptionHolderOpen = false;
        this.initializeWrapper();
        this.formState = {
            formTitle : "",
            formDescription:"",
            formAuthor:"",
            formFields:[]
        }
    }

    handleStateChange(e){
        this.formState[e.target.name] = e.target.value;
    }

    // will be useful when using templates
    initializeWrapper(){
        this.formWrapper.insertAdjacentHTML('beforeend',`
            <div id="form">
                <div class="form-element">
                    <input  id="form-title" name="formTitle" placeholder="Title" />
                    <input  id="form-description" name="formDescription" placeholder="Form Description" />
                </div>
            </div>
            <div id="field-creator" class="form-element">
                <div id="add-section-btn">
                    <img class="actions-btn" src="./resources/images/plus-lg.svg">
                </div>
                <div id="new-field-option-holder">
                    <div id="add-input-field-btn" class="actions-btn">
                        <img src="./resources/images/icons/input.svg">
                    </div>
                    <div id="add-mcq-field-btn" class="actions-btn">
                        <img  src="./resources/images/icons/checkbox.svg">
                    </div>
                    <div class="actions-btn">
                        <img  src="./resources/images/icons/linear-scale.svg">
                    </div>
                </div>
                
            </div>
            <div id="form-footer">
                <button id="publish-form-btn"> 
                    Publish
                </button>
            </div>
           
        `)
        document.getElementById("form-title").onchange = (e)=>{
            this.handleStateChange(e)
        }
        document.getElementById("form-description").onchange = (e)=>{
            this.handleStateChange(e)
        }
        document.getElementById("add-section-btn").onclick = this.toggleFieldCreatorOptionHolder.bind(this);
        document.getElementById("add-input-field-btn").onclick = this.addInputField.bind(this);
        document.getElementById("add-mcq-field-btn").onclick = this.addMCQField.bind(this);
        document.getElementById("publish-form-btn").onclick = () => {
            console.log("publishing form...")
            this.publishForm().then(res=>{

            }).catch(err=>{

            })
        }

    }

    getRandomId(){
        let chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz'.split('');
        let str = '';
        for (let i = 0; i < 16; i++) {
            str += chars[Math.floor(Math.random() * chars.length)];
        }
        return str;
    }

    toggleFieldCreatorOptionHolder(){
        document.getElementById('new-field-option-holder').style.display = (this.isFieldCreatorOptionHolderOpen?"none":"flex");
        this.isFieldCreatorOptionHolderOpen = !this.isFieldCreatorOptionHolderOpen;
    }

    addInputField(){
        console.log(this)
        const fieldId = this.getRandomId();
        const form = document.getElementById('form');
        const inputField = document.createElement("div");
        inputField.id = fieldId;
        inputField.classList.add("form-element");
        inputField.setAttribute("data-field-type","input");
        const fieldQuestion = document.createElement("input");
        fieldQuestion.classList.add("field-question")
        fieldQuestion.placeholder = "Untitled Question";
        const fieldContentPreview = document.createElement("input");
        fieldContentPreview.classList.add("field-content-preview")
        fieldContentPreview.placeholder = "Your answer here";
        fieldContentPreview.setAttribute("disabled",true)

        // Cross button to remove Field
        let dismisBtn = document.createElement("div")
        dismisBtn.classList.add("field-dismiss-btn")
        dismisBtn.onclick = () =>{
            this.formState.formFields = this.formState.formFields.filter(field=>field.fieldId!==fieldId)
            inputField.remove();
        }
        dismisBtn.innerHTML = `
            <img width="100%" src="./resources/images/icons/x-circle-fill.svg">
        `;
        inputField.appendChild(dismisBtn)


        // adding onchange handler to field question
        fieldQuestion.onchange = (e) => {
            const field = this.formState.formFields.filter(field=>field.fieldId===fieldId);
            field[0].fieldQuestion = e.target.value
            console.log(this.formState)
        }
        inputField.appendChild(fieldQuestion)
        inputField.appendChild(fieldContentPreview)
        form.appendChild(inputField)
        // adding field to the form state
        this.formState.formFields.push({
            fieldId,
            fieldType : "input",
            fieldQuestion : "",
            fieldContent : null
        })
    }
    addMCQField(){
        const fieldId = this.getRandomId();
        const form = document.getElementById('form');
        const mcqField = document.createElement("div");
        mcqField.id = fieldId;
        mcqField.classList.add("form-element");
        mcqField.setAttribute("data-field-type","mcq");
        const fieldQuestion = document.createElement("input");

        let dismisBtn = document.createElement("div")
        dismisBtn.classList.add("field-dismiss-btn")
        dismisBtn.onclick = () =>{
            this.formState.formFields = this.formState.formFields.filter(field=>field.fieldId!==fieldId)
            mcqField.remove();
        }
        dismisBtn.innerHTML = `
            <img width="100%" src="./resources/images/icons/x-circle-fill.svg">
        `;
        mcqField.appendChild(dismisBtn)

        fieldQuestion.classList.add("field-question")
        fieldQuestion.placeholder = "Untitled Question";
        const mcqOptionWrapper = document.createElement("div")
        mcqOptionWrapper.classList.add("mcq-option-wrapper")
        const optionActions = document.createElement("div");
        optionActions.classList.add("add-option");
        const addOptionBtn = document.createElement("img");
        addOptionBtn.classList.add("actions-btn");
        addOptionBtn.src = "./resources/images/plus-lg.svg";
        const removeOptionBtn = document.createElement("img");
        removeOptionBtn.classList.add("actions-btn");
        removeOptionBtn.src = "./resources/images/icons/trash-fill.svg";
        optionActions.appendChild(addOptionBtn)
        optionActions.appendChild(removeOptionBtn)
        mcqField.appendChild(fieldQuestion)
        mcqField.appendChild(mcqOptionWrapper)
        mcqField.appendChild(optionActions)
        form.appendChild(mcqField)

        


        // The div above looks like this
        //
        // <div id="${fieldId}" class="form-element">
        //     <input class="field-question" placeholder="Enter your question here"/>
        //     <div class="mcq-option-wrapper">
        //     </div>
        //     <div class="add-option">
        //         <img class="actions-btn" src="./resources/images/plus-lg.svg">
        //         <img class="actions-btn" src="./resources/images/icons/trash-fill.svg">
        //     </div>
        // </div>
        fieldQuestion.onchange = (e) => {
            const field = this.formState.formFields.filter(field=>field.fieldId===fieldId);
            field[0].fieldQuestion = e.target.value
            console.log(this.formState)
        }
       addOptionBtn.onclick = ()=>{
            this.addMCQOption(fieldId);
        };
        removeOptionBtn.onclick = ()=>{
            this.removeMCQOption(fieldId);
        };
        this.formState.formFields.push({
            fieldId,
            fieldType : "mcq",
            fieldQuestion : "",
            fieldContent : {
                options : []
            }
        })        
    }

    addMCQOption(fieldId){
        const formElement = document.getElementById(fieldId);
        let mcqOptionHolder = document.createElement("div");
        mcqOptionHolder.classList.add("mcq-option-holder");
        let optionInput = document.createElement("input");
        optionInput.classList.add("mcq-option");
        optionInput.placeholder = "New Option";
        // adding onchange events to option
        optionInput.onchange = (e) => {
            let field = this.formState.formFields.filter(field=>field.fieldId===fieldId);
            field = field[0];
            field.fieldContent.options[Number(e.target.getAttribute("data-option-number"))].value = e.target.value
        }
        mcqOptionHolder.appendChild(optionInput);
        formElement.getElementsByClassName("mcq-option-wrapper")[0].appendChild(mcqOptionHolder);

        let field = this.formState.formFields.filter(field=>field.fieldId===fieldId);
        field = field[0];
        field.fieldContent.options.push({
            value : ""
        })
        optionInput.setAttribute("data-option-number",field.fieldContent.options.length-1);
    }

    removeMCQOption(fieldId){
        const options = document.getElementById(fieldId).getElementsByClassName("mcq-option-wrapper")[0].children;
        if(options.length){
            const toRemoveOption = options[options.length-1]
            toRemoveOption.remove();
        }
        let field = this.formState.formFields.filter(field=>field.fieldId===fieldId);
        field = field[0];
        field.fieldContent.options.pop()
    }


    addRangeField(){

    }

    validateForm(){
        console.log("validating form...")
        if(
            !this.formState.formTitle ||
            !this.formState.formDescription ||
            !this.formState.formFields.length){
                console.log("invalid form!")
                return false;
            }
        return true;
    }

    async publishForm(){
        try{
            if(!this.validateForm()){
                createToast({
                    heading: "Invalid Form",
                    message : "Please fill empty fields"
                });
                return;
            }
            return await server.post({
                route: "/api/create-form",
                body: {
                    formData : {
                        ...this.formState,
                        formAuthor : localStorage.getItem('user')
                    }
                }
            }).then(res=>{
                if(res.status===200){
                    window.location = "/index.html"
                } else{
                    createToast({
                        heading: res.status + ": Error in submission",
                        message : res.message
                    });  
                }
            })
        }catch(e){
            console.log(e)
        }
    }

}

export default Form;

