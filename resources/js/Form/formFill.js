import server from "../mockServer/server.js"
import {getLoginInfo} from "../utils.js"
import { createToast } from "../toasts.js";

class FormFiller{
    constructor(formWrapper,formId){
        this.formWrapper = formWrapper;
        this.formId = formId;
        this.formResponse = {
            formParticipant:getLoginInfo(),
            formId : formId
        }
        this.fetchForm().then(res=>{
            this.initializeForm();
        })
    }
    async fetchForm(){
        server.get({
            route:"/api/get-form",
            params:{
                "formId": this.formId
            }
        }).then(res=>{
            this.formData = res.data.form;
        })
    }
    initializeForm(){
        const formData = this.formData;
        this.formWrapper.insertAdjacentHTML('beforeend',`
            <div class="form-title">
                ${formData.formTitle}
            </div>
            <div class="form-description">
                ${formData.formDescription}
            </div>
            <div class="form-holder">
            </div>
            <button id="submit-form-btn"> 
                Submit
            </button>
        `)

        let formHolder = this.formWrapper.getElementsByClassName("form-holder")[0];
        let responseData = {
            fields:[]
        }
        this.formResponse.responseData = responseData;
        for(let [idx,field] of formData.formFields.entries()){
            responseData.fields.push({
                fieldId: field.fieldId,
                fieldValue: null,
                fieldType:field.fieldType
            })
            formHolder.insertAdjacentHTML('beforeend',`
                <div class="form-element"></div>
            `)
            let formElements = formHolder.getElementsByClassName("form-element");
            let formElement = formElements[formElements.length-1];
            switch(field.fieldType){
                case "input":
                    this.insertInputField(formElement,field,idx);
                    break;
                case "mcq":
                    this.insertMCQField(formElement,field,idx);

            }
        }
        let submitBtn = document.getElementById("submit-form-btn");
        submitBtn.onclick = ()=>{
            submitBtn.disabled = true;
            this.submitForm();
        }
    }

    insertInputField(formElement,field,idx){
        let inputQuestion = document.createElement("div");
        inputQuestion.innerHTML = `<span>${(idx+1)}. ${field.fieldQuestion}</span>`;
        inputQuestion.classList.add("input-question");

        let inputField = document.createElement("input");
        inputField.classList.add("input-field");
        inputField.id = field.fieldId;

        formElement.appendChild(inputQuestion);
        formElement.appendChild(inputField);

        inputField.onchange = (e)=>{
            let activeField = (this.formResponse.responseData.fields.filter(f=>f.fieldId===field.fieldId))[0];
            activeField.fieldValue = e.target.value;
        }
    }

    insertMCQField(formElement,field,idx){
        let inputQuestion = document.createElement("div");
        inputQuestion.innerHTML = `<span>${(idx+1)}. ${field.fieldQuestion}</span>`;
        inputQuestion.classList.add("input-question");

        let optionHolder = document.createElement("div");
        for(let [optionIdx, option]  of field.fieldContent.options.entries()){
            let optionElement = document.createElement("div");
            optionElement.setAttribute('data-value',optionIdx)
            optionElement.innerHTML = `<span>${(optionIdx+1)}. ${option.value}</span>`;
            optionElement.classList.add("input-option");
            optionHolder.appendChild(optionElement);

            optionElement.onclick = (e) =>{
                let activeField = (this.formResponse.responseData.fields.filter(f=>f.fieldId===field.fieldId))[0];
                activeField.fieldValue = optionIdx;
                optionHolder.childNodes.forEach(child=>{
                    (child.getAttribute('data-value')!==String(optionIdx))?
                    child.removeAttribute('data-selected'):
                    child.setAttribute('data-selected',true)
                })
            }
        }

        formElement.appendChild(inputQuestion);
        formElement.appendChild(optionHolder);
    }

    async submitForm(){
        let submitBtn = document.getElementById("submit-form-btn");
        try{
            for(let field of this.formResponse.responseData.fields){
                if(field.fieldValue === null){
                    return createToast({
                        heading:"Incomplete Form",
                        message:"Please fill all the form details"
                    })
                }
            }

            await server.post({
                route: "/api/submit-response",
                body:{
                    formResponse:this.formResponse
                }
            }).then(res=>{
                if(res.status!==200){
                    throw new Error(res.message);
                }
                window.location = "/index.html?isSubmissionRedirect=1";
            });
        }catch(err){
            createToast({
                heading:"Submission Error",
                message:err.message
            });
            submitBtn.disabled = false;
        }
    }

}

export default FormFiller;

