import server from "../mockServer/server.js";
import { createToast } from "../toasts.js";
import { getLoginInfo } from "../utils.js";

async function initializeForms(){
    const forms = await server.get({
        route:"/api/get-all-forms",
    }).then(res=>{
        if(res.status!==200){
            createToast({
                heading: err.status+" : Error in fetching",
                message:err.message
            })
            return [];
        }
        return res.data.forms;
    }).catch(err=>{
       console.log(err);
       return [];
    })
    const user = getLoginInfo();

    const othersFormSection = document.getElementById('others-forms-section');
    othersFormSection.insertAdjacentHTML('beforebegin',`
        <div class="form-section__section-title">Others Forms</div>
    `);
    Object.keys(forms).forEach(formId=>{
        const form = forms[formId];
        if(form.formAuthor===user) return;
        othersFormSection.insertAdjacentHTML('beforeend',`
        <div id="${formId}" class="form-card form-posted">
            <div class="details">
            <div class="form-posted-title">
                ${form.formTitle}
            </div>
            <div class="form-posted-body">
                ${form.formDescription}
            </div>
            <div class="form-posted-author">
                Created by : ${form.formAuthor}
            </div>
            </div>
            <div class="actions">
            ${user===form.formAuthor?
            `
            <img class="actions-btn" src="./resources/images/pencil.svg">
            <img class="actions-btn" src="./resources/images/icons/bar-chart-fill.svg">
            `:``}
            <img class="actions-btn" src="./resources/images/eye-fill.svg" onclick="(()=>{
                window.location = '/fillForm.html?formId=${formId}'
            })()">
            </div>
        </div>
        `)
    })  
    let otherForms = Object.keys(forms).filter(formId=>{
        return forms[formId].formAuthor!==user;
    })
    if(!otherForms.length){
        othersFormSection.insertAdjacentHTML('beforebegin',`
            <div class="form-section__no-form-display"><img src="./resources/images/icons/file-earmark-bar-graph-fill.svg"/>No forms to display</div>
        `);
    }


    const myFormSection = document.getElementById('my-forms-section');
    myFormSection.insertAdjacentHTML('beforebegin',`
        <div class="form-section__section-title">My Forms</div>
    `);
    Object.keys(forms).forEach(formId=>{
        const form = forms[formId];
        if(form.formAuthor!==user) return;
        myFormSection.insertAdjacentHTML('beforeend',`
        <div id="${formId}" class="form-card form-posted">
            <div class="details">
            <div class="form-posted-title">
                ${form.formTitle}
            </div>
            <div class="form-posted-body">
                ${form.formDescription}
            </div>
            <div class="form-posted-author">
                Created by : ${form.formAuthor}
            </div>
            </div>
            <div class="actions">
            ${user===form.formAuthor?
            `
            <img class="actions-btn" src="./resources/images/pencil.svg">
            <img class="actions-btn" src="./resources/images/icons/bar-chart-fill.svg" onclick="(()=>{
                window.location = '/viewResponses.html?formId=${formId}';
            })()">
            `:``}
            <img class="actions-btn" src="./resources/images/eye-fill.svg" onclick="(()=>{
                window.location = '/fillForm.html?formId=${formId}'
            })()">
            </div>
        </div>
        `)
    })  

    let myForms = Object.keys(forms).filter(formId=>{
        return forms[formId].formAuthor===user;
    })
    if(!myForms.length){
        myFormSection.insertAdjacentHTML('beforebegin',`
            <div class="form-section__no-form-display"><img src="./resources/images/icons/file-earmark-bar-graph-fill.svg"/> No forms to display</div>
        `);
    }
}


export {
    initializeForms
}