import { getLoginInfo } from "./utils.js";

function Navbar(){
    const user = getLoginInfo();
    const navbar = document.getElementsByTagName("nav")[0];
    navbar.insertAdjacentHTML('beforeend',`
        <div id="logo-area">
            <img src="./resources/images/logo.png" class="logo"/>
        </div>
        ${(user)?
            `
            <a href="/index.html" class="nav-link nav-link--first">Home</a>
            <a href="/createForm.html" class="nav-link nav-link--last">Create</a>
            <div class="profile-info">
                <div class="profile-info__username">Hi ${user}!</div>
                <button onclick="(()=>{
                    localStorage.removeItem('user');
                    window.location='/index.html';
                })()" class="profile__logout">Logout</button>
                
            </div>`
            :
            `<div class="profile-info">
                <button onclick="(()=>{
                    window.location='/loginPage.html';
                })()" class="profile__logout">Login</button>
            </div>`
        }
    `);
}

{/* <div id="logo-area">
<img src="./resources/images/logo.png" class="logo"/>
</div>
<!-- <div class="search">
<button class="search-btn">
    <img class="search-icon"  src="./resources/images/search-btn.png"/>
</button>
<input name="search-text" placeholder="Search"/>
</div> -->
<img src="./resources/images/profile-pic.png" class="profile-info"/> */}

export {
    Navbar
}