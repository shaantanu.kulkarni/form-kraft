function createToast({
    heading,
    message
}){
    const toastElement = document.createElement("div")
    toastElement.classList.add("toast")
    document.getElementsByTagName("body")[0].appendChild(toastElement);
    const toastDismissible = document.createElement("img")
    toastDismissible.src = "/resources/images/icons/x-circle-fill.svg";
    toastDismissible.classList.add("toast__dismissible")
    toastElement.appendChild(toastDismissible);

    toastElement.insertAdjacentHTML('beforeend',`
        <div class="toast__heading">
            ${heading}
        </div>
        <div class="toast__message">
            ${message}
        </div>
    `)
    const timeout = setTimeout(()=>{
        toastElement.remove();
    },3000);

    toastDismissible.onclick = () =>{
        clearTimeout(timeout);
        toastElement.remove();
    }
}

export {
    createToast
}