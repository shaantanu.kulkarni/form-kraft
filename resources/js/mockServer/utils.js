function response(error,message,data,status){
    this.error = error;
    this.message = message;
    this.data = data;
    this.status = status;    
}

function getRandomId(){
    let chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz'.split('');
    let str = '';
    for (let i = 0; i < 16; i++) {
        str += chars[Math.floor(Math.random() * chars.length)];
    }
    return str;
}


export {
    getRandomId,
    response
}