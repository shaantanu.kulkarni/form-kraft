import { getRandomId, response } from "./utils.js"

function createNewForm(formData){
    // form validation not implemented because not in scope of frontend
    try{
        const formId = getRandomId();
        let forms = localStorage.getItem('forms')?JSON.parse(localStorage.getItem('forms')):{};
        forms[formId] = formData;
        localStorage.setItem('forms',JSON.stringify(forms))
        return new response(false,"successfully created form",{
            ...formData,
            id:formId  
        },200)
    } catch(err){
        console.log(err)
        return new response(true,err.message,{},400)
    }
}
function getAllForms(){
    try{
        let forms = localStorage.getItem('forms')?JSON.parse(localStorage.getItem('forms')):{};
        return new response(false,"retrieved forms",{
            forms 
        },200)
    } catch(err){
        return new response(true,err.message,{},400)
    }
}
function getForm(formId){
    try{
        let forms = localStorage.getItem('forms')?JSON.parse(localStorage.getItem('forms')):{};
        let form = forms[formId];
        if(!form){
            return new response(true,"Invalid form Id",{},400)
        }
        return new response(false,"retrieved form",{
            form
        },200)
    } catch(err){
        console.log(err)
        return new response(true,err.message,{},500)
    }
}

function submitResponse(formResponse){
    try{
        const formId = formResponse.formId;
        let responses = localStorage.getItem('responses')?JSON.parse(localStorage.getItem('responses')):{};
        if(!responses[formId]){
            responses[formId] = [];
        }
        responses[formId].push(formResponse)
        localStorage.setItem('responses',JSON.stringify(responses))
        return new response(false,"successfully submitted form",{},200)
    } catch(err){
        console.log(err)
        return new response(true,err.message,{},500)
    }
}

function getResponses(formId){
    try{
        let formData = (localStorage.getItem('forms')?JSON.parse(localStorage.getItem('forms')):{})[formId];
        if(!formData){
            return new response(true,"Invalid form Id",{},400)
        }
        let responses = localStorage.getItem('responses')?JSON.parse(localStorage.getItem('responses')):{};
        return new response(false,"successfully retrived responses",{
            formData,
            responses:responses[formId]??[]
        },200)
    } catch(err){
        return new response(true,err.message,{},500)
    }
}

export {
    createNewForm,
    getAllForms,
    getForm,
    submitResponse,
    getResponses
}