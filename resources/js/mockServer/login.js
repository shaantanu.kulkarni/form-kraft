import {response } from "./utils.js"

function loginUser(username){
    try{
        if(!username.length){
            throw new Error("Invalid Username")
        }
        localStorage.setItem('user',username);
        return new response(false,"User logged in successfully.",{
        },200)
    } catch(err){
        return new response(true,err.message,{},400)
    }
}

export {
    loginUser
}