import { createNewForm, getAllForms,getForm , submitResponse , getResponses } from "./formCruds.js";
import { loginUser } from "./login.js";
import { response } from "./utils.js";

class serverObject{
    async post({
        route,
        body,
        headers,
        options
    }){
        switch (route) {
            case "/api/create-form":
                return createNewForm(body.formData);
            case "/api/submit-response":
                return submitResponse(body.formResponse);
            
            case "/api/login":
                return loginUser(body.username);
            default:
                return new response(true,'route not found',{}, 400)
        }
    }
    async get({
        route,
        params,
        options
    }){
        switch (route) {
            case "/api/get-all-forms":
                return getAllForms();

            case "/api/get-form":
                return getForm(params.formId);

            case "/api/get-responses":
                return getResponses(params.formId);
            default:
                return new response(true,'route not found',{}, 400)
        }
    }

}

const server = new serverObject();

export default server;