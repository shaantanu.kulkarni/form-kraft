function getLoginInfo(){
    // you can use the mock server here but I'm using the local storage directly. Because we are just mocking the authentication process.
    const user = localStorage.getItem('user')??null;
    return user;
}

export {
    getLoginInfo
}